import java.util.Map;
import java.util.TreeMap;

import unidad8.colecciones.adicionales.Colecciones1;

public class Ejercicio8 {

	public static Map<String, Integer> interseccion(Map<String, Integer> mapa1, Map<String, Integer> mapa2) {

		Map<String, Integer> mapainterseccion = new TreeMap<String, Integer>();

		for (String key : mapa1.keySet()) {
			if (mapa2.containsKey(key))
				mapainterseccion.put(key, mapa1.get(key));
		}
		return mapainterseccion;

	}

	
	 
	public static void main(String[] args) {
		
		  Map<String, Integer> mapa1 = new TreeMap<String, Integer>();
		  mapa1.put("Fernando", 56);
		  mapa1.put("Lucia", 22);
		  mapa1.put("Roberto", 78);
		  mapa1.put("Marta", 43);
		  mapa1.put("Recaredo", 65);
		  mapa1.put("Ana", 86);
		  
		  
		  System.out.println("");
		  	  	  
		// Imprimimos mapa1 con un Iterador
		  
		      System.out.println("primer mapa:");
			
		      System.out.println("");
		  
		mapa1.forEach((nombre,edad)->System.out.println("nombre:" + nombre+ " - "+ "edad:"+ edad));
		
		       System.out.println("");
		
		      System.out.println("segundo mapa:");
		
		      System.out.println("");
			  
		  	
		  Map<String, Integer> mapa2 = new TreeMap<String, Integer>();
		  mapa2.put("Carlos", 54);
		  mapa2.put("Lucia", 22);
		  mapa2.put("Roberto", 78);
		  mapa2.put("Enrique", 35);
		  mapa2.put("Oscar", 59);
		  mapa2.put("Ana", 86);
		  
		// Imprimimos mapa2 con un Iterador
		  
		  mapa2.forEach((nombre,edad)->System.out.println("nombre:" + nombre+ " - "+ "edad:"+ edad)); 
		  
		
		  Map<String, Integer> mapa3 = new TreeMap<String, Integer>();
		  
		  mapa3 = Colecciones1.interseccion(mapa1, mapa2);  
		  
		        System.out.println("");
		  
		        System.out.println("mapa interseccion:");
		  
		  mapa3.forEach((nombre,edad)->System.out.println("nombre:" + nombre+ " - "+ "edad:"+ edad));

	}

}
