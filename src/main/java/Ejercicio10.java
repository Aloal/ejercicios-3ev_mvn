import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import unidad8.colecciones.adicionales.Colecciones1;

public class Ejercicio10 {
	

	 public static HashMap<List<String>, HashMap<String,Integer>>  cuentaPares(List<String>palabras) {
		 
		 
			HashMap<List<String>, HashMap<String,Integer>> total = new HashMap<List<String>,HashMap<String, Integer>>();
			
			 for(int i=0;i<palabras.size();i++) {
				 String silaba="";
				 String palabra=palabras.get(i);
				 ArrayList<String>silabas=new ArrayList<String>();
				 
				 for(int j=0; j<palabra.length()-1; j++ ) {
					 
					 silaba+=palabra.charAt(j);
					 silaba+=palabra.charAt(j+1);
					 silabas.add(silaba);
					 silaba="";
				 }
					 
			    HashMap<String,Integer>apariciones=new  HashMap<String,Integer>();
			    
			     for(String pares: silabas) {
			    	 
			    	 if(apariciones.containsKey(pares)) {
			    		 apariciones.put(pares,apariciones.get(pares)+1);
			    	 }
			    	 
			    	 else {
			    		 apariciones.put(pares, 1);
			    	 }
			    	 
			     }
			
			     total.put(silabas, apariciones);
			 }
			
			
			   return total;
			 }
		 
	 
	

	public static void main(String[] args) {
		
		 List<String> cadenas= new ArrayList<String>();
			
			cadenas.add("banana");
			cadenas.add("melon");
			cadenas.add("pera");
			
			System.out.println(cuentaPares(cadenas));

	}

}
